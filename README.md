# Recipes-Vue-App

# The project is deployed and available to view [HERE](https://marcinmroz.me/recipes)

This project was build with **Vue.JS** and employs tools such as **Axios** and **VueX**. The main idea behind the project is to display a range of recipes that are pulled from freemealdb.
The app accesses assets from the **REST API** and utilizes local storage with **Vue Persist**. The main functionality allows the user to view a whole list of recipes with images, search by characters, filter by tags, area and category. Moreover, the user is able to add/remove to/from favourites, and expand each one of the recipes in order to see the instructions and a YouTube (embedded player) video on how to prepare the meal in question.

Last but not least, I used tools such as **Docker** and **NGINX** to contenerise the project and create a server with reverse proxy to serve in production mode to my [DigitalOcean droplet](https://marcinmroz.me/recipes)

## Stack:

- Vue v2
- Vuex
- SCSS
- Axios (Accessing the REST API)
- Vue Persist (Local storage handling)
- JavaScript

## Demo:

![Demo](src/assets/demo/recipes-demo.mp4)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
